var pictureArray = ['dog','mouse','cat','frog','bug','fly','spider','bat','monkey'];
var pictureToGuess = randomPicture();

window.addEventListener("load", init);

function init(){
    showPictures();
    showPicturesToGuess();
}

function showPictures(){
    //<img src="cat.jpg" id="cat" class="img--small"
    for(var index in pictureArray){
        var img = document.createElement('img');
        img.setAttribute('src', 'start/libs/img/'+pictureArray[index]+'.jpg');
        img.setAttribute('class', 'img--small');
        img.setAttribute('id', pictureArray[index])
        img.addEventListener('click', handleImgClickEvent);
        var ph = document.getElementById('img-grid');
        ph.appendChild(img);
    }
}

function handleImgClickEvent(event){
    console.log(event.target.id);
    var chosenPicture = event.target.id;
    if(chosenPicture = pictureToGuess){
        console.log('Yeah, goed geraden!');
        document.getElementById('message').innerHTML = 'Goed geraden!';
        pictureToGuess = randomPicture();
        showPicturesToGuess();
    }else{
        console.log('Jammer!');
        document.getElementById('message').innerHTML = 'Dat is niet goed!';
    }
}

function showPicturesToGuess(){
    var img = document.getElementById('random-img');
    img.setAttribute('src', 'start/libs/img/'+pictureToGuess+'.jpg')
    }

function randomPicture(){
    //console.log(Math.floor(pictureArray.length * Math.random()));
    return pictureArray[Math.floor(pictureArray.length * Math.random())];
}